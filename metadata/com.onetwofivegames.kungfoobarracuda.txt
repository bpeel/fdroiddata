Categories:Games
License:GPLv3
Web Site:https://twitter.com/onetwofivegames
Source Code:https://github.com/poseidn/KungFoo
Issue Tracker:https://github.com/poseidn/KungFoo/issues

Auto Name:Kung Foo Barracuda
Summary:Play a boot'em up arcade game
Description:
Beat'em up arcade for the masses!
.

Repo Type:git
Repo:https://github.com/poseidn/KungFoo.git

Build:1.0,1
    commit=release-1.0
    subdir=build_android
    init=cd .. && \
        ./distributeAssets.sh && \
        cd build_android && \
        ./get_externals.sh
    target=android-16
    buildjni=yes

Maintainer Notes:
Downloads files during build; check shell scripts before updating.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
